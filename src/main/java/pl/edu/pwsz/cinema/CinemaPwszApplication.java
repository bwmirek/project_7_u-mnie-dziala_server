package pl.edu.pwsz.cinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Główna klasa uruchamiające aplikację/serwer
 */
@SpringBootApplication
public class CinemaPwszApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaPwszApplication.class, args);
    }

}
