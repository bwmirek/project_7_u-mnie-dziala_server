package pl.edu.pwsz.cinema.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Klasa do konfiguracji aplikacji
 */
@Configuration
@EnableSwagger2
public class Config {
}
