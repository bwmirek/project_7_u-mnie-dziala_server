package pl.edu.pwsz.cinema.repertoire.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pwsz.cinema.repertoire.dto.MovieDTO;
import pl.edu.pwsz.cinema.repertoire.service.MovieService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Klasa odierająca zapytania od klienta
 */
@RestController
@RequiredArgsConstructor
public class MovieController {

    private final MovieService movieService;

    /**
     * Klasa wyświetlająca wszystkie aktualnie grane filmy
     *
     * @return zwraca listę filmów
     */
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/movies", produces = "application/json; charset=UTF-8")
    public List<MovieDTO> getMovies() {
        return movieService.getMovies()
                .stream()
                .map(MovieDTO::from)
                .collect(Collectors.toList());
    }
}
