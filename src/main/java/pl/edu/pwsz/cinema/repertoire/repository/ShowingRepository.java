package pl.edu.pwsz.cinema.repertoire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;

/**
 * Repozytorium do wybierania seansów z bazy
 */
@Repository
public interface ShowingRepository extends JpaRepository<Showing, Integer> {
}
