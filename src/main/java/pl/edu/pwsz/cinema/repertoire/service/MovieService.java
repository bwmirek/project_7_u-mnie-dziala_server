package pl.edu.pwsz.cinema.repertoire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.repository.MovieRepository;

import java.util.List;

/**
 * Pobranie danych o filmach z repozytorium
 */
@Service
@RequiredArgsConstructor
public class MovieService {

    /**
     * Repozytorium reprezentujące filmy
     */
    private final MovieRepository movieRepository;

    /**
     * Pobranie listy filmów z repozytorium
     *
     * @return zwraca wszystkie znalezione filmy
     */
    public List<Movie> getMovies() {
        return movieRepository.findAll();
    }

    public Movie getMovie(int id) {
        return movieRepository.findById(id).orElseThrow();
    }

    public void save(Movie movie) {
        movieRepository.save(movie);
    }

    public void delete(Movie movie) {
        movieRepository.delete(movie);
    }
}
