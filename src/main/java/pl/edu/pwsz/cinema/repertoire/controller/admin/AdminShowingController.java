package pl.edu.pwsz.cinema.repertoire.controller.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;
import pl.edu.pwsz.cinema.repertoire.service.MovieService;
import pl.edu.pwsz.cinema.repertoire.service.RoomService;
import pl.edu.pwsz.cinema.repertoire.service.ShowingService;

@Controller
@RequestMapping(value = "/admin/showings")
@RequiredArgsConstructor
public class AdminShowingController {

    private final ShowingService showingService;
    private final RoomService roomService;
    private final MovieService movieService;

    @GetMapping(value = "/list")
    public String list(Model model) {
        model.addAttribute("showings", showingService.getShowings());

        return "admin/showings/list";
    }

    @GetMapping(value = "/edit")
    public String edit(@RequestParam int id, Model model) {
        model.addAttribute("showing", showingService.getShowing(id));
        model.addAttribute("rooms", roomService.getRooms());
        model.addAttribute("movies", movieService.getMovies());

        return "admin/showings/edit";
    }

    @PostMapping(value = "/edit")
    public RedirectView submitEdit(@ModelAttribute Showing showing) {
        showingService.save(showing);

        return new RedirectView("/admin/showings/list");
    }

    @GetMapping(value = "/add")
    public String add(Model model) {
        model.addAttribute("showing", new Showing());
        model.addAttribute("rooms", roomService.getRooms());
        model.addAttribute("movies", movieService.getMovies());
        return "admin/showings/add";
    }

    @PostMapping(value = "/add")
    public RedirectView submitAdd(@ModelAttribute Showing showing) {
        showingService.save(showing);

        return new RedirectView("/admin/showings/list");
    }

    @GetMapping(value = "/delete")
    public RedirectView delete(@RequestParam int id) {
        Showing showing = showingService.getShowing(id);
        if (!showing.getTickets().isEmpty()) {
            return new RedirectView("/admin/showings/error");
        }

        showingService.delete(showing);

        return new RedirectView("/admin/showings/list");
    }

    @GetMapping(value = "/error")
    public String error() {
        return "/admin/showings/error";
    }
}
