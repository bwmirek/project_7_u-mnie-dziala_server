package pl.edu.pwsz.cinema.repertoire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;

/**
 * Repozytorium do wybierania filmów z bazy
 */
@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
