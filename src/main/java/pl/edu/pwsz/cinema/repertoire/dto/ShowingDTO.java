package pl.edu.pwsz.cinema.repertoire.dto;

import lombok.Builder;
import lombok.Getter;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;

import java.time.LocalDateTime;

/**
 * Obiekt zawierający informacje o danym seansie
 */
@Getter
@Builder
public class ShowingDTO {
    /**
     * Unikalny identyfikator seansu
     */
    private final int id;
    /**
     * Czas grania seansu
     */
    private final LocalDateTime playingDate;
    /**
     * Dostępne miejsca zwykłe na seans
     */
    private final int seats;
    /**
     * Dostępne miejsca vip na seans
     */
    private final int vipSeats;
    /**
     * Ilość aktualnie zakupionych biletów
     */
    private final int tickets;
    /**
     * Ilość aktualnie zakupionych biletów vip
     */
    private final int vipTickets;
    /**
     * Unikalny identyfikator filmu
     */
    private final int movie;

    /**
     * Metoda do tworzenia obiektu DTO z podanego seansu
     *
     * @param showing obiekt wybranego filmu
     * @return zwraca obiekt DTO
     */
    public static ShowingDTO from(Showing showing) {
        return ShowingDTO.builder()
                .id(showing.getId())
                .playingDate(showing.getPlayingDate())
                .seats(showing.getRoom().getSeats())
                .vipSeats(showing.getRoom().getVipSeats())
                .tickets(showing.countTickets())
                .vipTickets(showing.getVipTicketsCount())
                .movie(showing.getMovie().getId())
                .build();
    }
}
