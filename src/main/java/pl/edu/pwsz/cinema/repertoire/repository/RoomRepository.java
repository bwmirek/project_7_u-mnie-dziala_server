package pl.edu.pwsz.cinema.repertoire.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pwsz.cinema.repertoire.entity.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {
}
