package pl.edu.pwsz.cinema.repertoire.controller.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.service.MovieService;

@Controller
@RequestMapping(value = "/admin/movies")
@RequiredArgsConstructor
public class AdminMovieController {

    private final MovieService movieService;

    @GetMapping(value = "/list")
    public String list(Model model) {
        model.addAttribute("movies", movieService.getMovies());

        return "admin/movies/list";
    }

    @GetMapping(value = "/edit")
    public String edit(@RequestParam int id, Model model) {
        model.addAttribute("movie", movieService.getMovie(id));

        return "admin/movies/edit";
    }

    @PostMapping(value = "/edit")
    public RedirectView submitEdit(@ModelAttribute Movie movie) {
        movieService.save(movie);

        return new RedirectView("/admin/movies/list");
    }

    @GetMapping(value = "/add")
    public String add(Model model) {
        model.addAttribute("movie", new Movie());

        return "admin/movies/add";
    }

    @PostMapping(value = "/add")
    public RedirectView submitAdd(@ModelAttribute Movie movie) {
        movieService.save(movie);

        return new RedirectView("/admin/movies/list");
    }

    @GetMapping(value = "/delete")
    public RedirectView delete(@RequestParam int id) {
        Movie movie = movieService.getMovie(id);
        if (!movie.getShowings().isEmpty()) {
            return new RedirectView("/admin/movies/error");
        }

        movieService.delete(movie);

        return new RedirectView("/admin/movies/list");
    }

    @GetMapping(value = "/error")
    public String error() {
        return "/admin/movies/error";
    }
}
