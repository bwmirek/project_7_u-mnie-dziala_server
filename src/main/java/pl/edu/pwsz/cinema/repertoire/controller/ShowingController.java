package pl.edu.pwsz.cinema.repertoire.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pwsz.cinema.repertoire.dto.ShowingDTO;
import pl.edu.pwsz.cinema.repertoire.service.ShowingService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Klasa odbierająca zapytania od klienta
 */
@RestController
@RequiredArgsConstructor
public class ShowingController {

    private final ShowingService showingService;

    /**
     * Klasa wyświetlająca wszystkie seanse który były ,są lub będą grane
     *
     * @return zwraca listę seansów
     */
    @CrossOrigin(origins = "*")
    @GetMapping(path = "/showings", produces = "application/json; charset=UTF-8")
    public List<ShowingDTO> getShowings() {
        return showingService.getShowings()
                .stream()
                .map(ShowingDTO::from)
                .collect(Collectors.toList());
    }
}
