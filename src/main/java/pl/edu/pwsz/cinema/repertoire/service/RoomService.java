package pl.edu.pwsz.cinema.repertoire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.pwsz.cinema.repertoire.entity.Room;
import pl.edu.pwsz.cinema.repertoire.repository.RoomRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;

    public List<Room> getRooms() {
        return roomRepository.findAll();
    }
}
