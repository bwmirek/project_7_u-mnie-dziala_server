package pl.edu.pwsz.cinema.repertoire.entity;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pwsz.cinema.commerce.entity.Ticket;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Klasa opisująca seans
 */
@Entity
@Getter
@Setter
public class Showing {
    /**
     * Unikalny identyfikator seansu
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    /**
     * Czas grania seansu
     */
    private LocalDateTime playingDate;

    /**
     * Połączenie do obiektu 'movie'
     */
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;
    /**
     * Połączenie do obiektu 'room'
     */
    @ManyToOne
    private Room room;
    /**
     * Połączenie do listy biletów (tickets)
     */
    @OneToMany(mappedBy = "showing")
    private List<Ticket> tickets;

    /**
     * Pole zawierające ilość kupionych biletów  (nie połączone z bazą danych)
     */
    @Transient
    private Integer ticketsCount = null;
    /**
     * Pole zawierające ilość kupionych biletów vip  (nie połączone z bazą danych)
     */
    @Transient
    private Integer vipTicketsCount = null;

    /**
     * Metoda obliczająca kupione bilety
     *
     * @return zwraca liczbę wykupionych biletów
     */
    @Transient
    public int countTickets() {
        if (ticketsCount == null) {
            calculateTickets();
        }

        return ticketsCount;
    }

    /**
     * Metoda obliczająca kupione bilety vip
     *
     * @return zwraca liczbę wykupionych biletów vip
     */
    @Transient
    public int countVipTickets() {
        if (vipTicketsCount == null) {
            calculateTickets();
        }

        return vipTicketsCount;
    }

    /**
     * Metoda obliczająca łączna ilość biletów i biletów vip
     */
    @Transient
    private void calculateTickets() {
        ticketsCount = 0;
        vipTicketsCount = 0;

        for (Ticket ticket : tickets) {
            if (ticket.isVip()) {
                vipTicketsCount++;
            } else {
                ticketsCount++;
            }
        }
    }
}
