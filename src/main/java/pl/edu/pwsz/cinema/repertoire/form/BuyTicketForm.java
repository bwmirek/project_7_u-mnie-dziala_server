package pl.edu.pwsz.cinema.repertoire.form;

import lombok.Getter;
import lombok.Setter;

/**
 * Klasa przetrzymująca dane wysłane od klienta do formularza
 */
@Getter
@Setter
public class BuyTicketForm {
    /**
     * Imię osoby kupującej
     */
    private String firstName;
    /**
     * Nazwisko osoby kupującej
     */
    private String lastName;
    /**
     * Wybrany seans
     */
    private int showing;
    /**
     *  Flaga oznaczająca czy bilet jest  vip
     */
    private boolean vip;
}
