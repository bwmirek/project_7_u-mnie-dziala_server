package pl.edu.pwsz.cinema.repertoire.dto;

import lombok.Builder;
import lombok.Getter;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;

/**
 * Obiekt zawierający informacje do wyświetlenia o danym filmie
 */
@Getter
@Builder
public class MovieDTO {
    /**
     * Unikalny identyfikator filmu
     */
    private final int id;
    /**
     * Tytuł filmu
     */
    private final String title;
    /**
     * Opis filmu
     */
    private final String description;
    /**
     * Grafika filmu
     */
    private final String cover;
    /**
     * Ocena filmu
     */
    private final int rating;
    /**
     * Cena biletu
     */
    private final int price;

    /**
     * Metoda do tworzenia obiektu DTO z podanego filmu
     *
     * @param movie  obiekt wybranego filmu
     * @return zwraca obiekt DTO
     */
    public static MovieDTO from(Movie movie) {
        return MovieDTO.builder()
                .id(movie.getId())
                .title(movie.getTitle())
                .description(movie.getDescription())
                .rating(movie.getRating())
                .cover(movie.getCover())
                .price(movie.getPrice())
                .build();
    }
}
