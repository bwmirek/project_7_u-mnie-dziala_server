package pl.edu.pwsz.cinema.repertoire.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Klasa opisująca sale
 */
@Entity
@Getter
@Setter
public class Room {
    /**
     * Unikalny identyfikator sali
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    /**
     * Liczba miejsc na sali
     */
    private int seats;
    /**
     * Liczba miejsc vip na sali
     */
    private int vipSeats;
}
