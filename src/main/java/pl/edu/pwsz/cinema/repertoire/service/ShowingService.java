package pl.edu.pwsz.cinema.repertoire.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;
import pl.edu.pwsz.cinema.repertoire.repository.ShowingRepository;

import java.util.List;

/**
 * Pobranie danych o seansach z repozytorium
 */
@Service
@RequiredArgsConstructor

public class ShowingService {

    /**
     * Repozytorium reprezentujące seanse
     */
    private final ShowingRepository showingRepository;

    /**
     * Pobranie listy filmów z repozytorium
     *
     * @return zwraca wszystkie znalezione filmy
     */
    public List<Showing> getShowings() {
        return showingRepository.findAll();
    }

    public Showing getShowing(int id) {
        return showingRepository.findById(id).orElseThrow();
    }

    public void save(Showing showing) {
        showingRepository.save(showing);
    }

    public void delete(Showing showing) {
        showingRepository.delete(showing);
    }
}
