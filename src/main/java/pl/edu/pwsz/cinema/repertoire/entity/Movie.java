package pl.edu.pwsz.cinema.repertoire.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Klasa opisująca film
 */
@Entity
@Getter
@Setter
public class Movie {
    /**
     * Unikalny identyfikator filmu
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;
    /**
     * Tytuł filmu
     */
    private String title;
    /**
     * Opis filmu
     */
    private String description;
    /**
     * Grafika filmu
     */
    private String cover;
    /**
     * Ocena filmu
     */
    private int rating;
    /**
     * Cena biletu
     */
    private int price;

    @OneToMany(mappedBy = "movie")
    private List<Showing> showings;
}
