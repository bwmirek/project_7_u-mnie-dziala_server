package pl.edu.pwsz.cinema.commerce.entity;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Klasa reprezentująca pojedynczy bilet w bazie danych
 */
@Entity
@Getter
@Setter
public class Ticket {
    /**
     * Unikalny identyfikator biletu
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * Czas zarezerwowania biletu
     */
    private LocalDateTime creationDate;
    /**
     * Imię osoby rezerwującej
     */
    private String firstName;
    /**
     * Nazwisko osoby rezerwującej
     */
    private String lastName;
    /**
     * Flaga oznaczająca czy bilet jest na miejsce vip
     */
    @Column(name = "is_vip")
    private boolean vip;

    /**
     * Zmienna oznaczająca na który seans jest dany bilet
     */
    @ManyToOne
    @JoinColumn(name = "showing_id")
    private Showing showing;
}
