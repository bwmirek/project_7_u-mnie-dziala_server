package pl.edu.pwsz.cinema.commerce.controller.admin;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pwsz.cinema.commerce.service.TicketService;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/admin/tickets")
public class AdminTicketController {

    private final TicketService ticketService;

    @GetMapping(value = "/list")
    public String list(Model model) {
        model.addAttribute("tickets", ticketService.getTickets());

        return "/admin/tickets/list";
    }
}
