package pl.edu.pwsz.cinema.commerce.controller;

import com.lowagie.text.DocumentException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import pl.edu.pwsz.cinema.commerce.entity.Ticket;
import pl.edu.pwsz.cinema.commerce.repository.TicketRepository;
import pl.edu.pwsz.cinema.repertoire.entity.Movie;
import pl.edu.pwsz.cinema.repertoire.entity.Showing;
import pl.edu.pwsz.cinema.repertoire.form.BuyTicketForm;
import pl.edu.pwsz.cinema.repertoire.repository.ShowingRepository;

import java.io.*;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Map;

/**
 * Klasa obługująca wszystkie zapytania wysłane na serwer , dotyczące biletów
 */
@RestController
@RequiredArgsConstructor
public class TicketController {

    private final ShowingRepository showingRepository;
    private final TicketRepository ticketRepository;

    /**
     * Ta metoda odbiera połączenie od klienta który chce kupić bilet
     *
     * @param form dane wysłane przez klienta
     */
    @CrossOrigin(origins = "*")
    @PostMapping(path = "/ticket", produces = "application/json; charset=UTF-8")
    public Map<String, Integer> createTicket(@RequestBody BuyTicketForm form) throws DocumentException, IOException {
        Showing showing = showingRepository.getById(form.getShowing());

        if (form.isVip()) {
            if (showing.countVipTickets() < showing.getRoom().getVipSeats()) {
                Ticket ticket = registerTicket(showing, form);

                return Collections.singletonMap("success", ticket.getId());
            }
        } else {
            if (showing.countTickets() < showing.getRoom().getSeats()) {
                Ticket ticket = registerTicket(showing, form);

                return Collections.singletonMap("success", ticket.getId());
            }
        }

        return Collections.singletonMap("success", 0);
    }

    /**
     * Ta metoda tworzy nowy bilet na konkretny seans i zapisuje go w bazie
     *
     * @param showing konkretny seans
     * @param form    dane wysłane przez klienta
     */
    private Ticket registerTicket(Showing showing, BuyTicketForm form) throws DocumentException, IOException {
        Ticket ticket = new Ticket();
        ticket.setShowing(showing);
        ticket.setFirstName(form.getFirstName());
        ticket.setLastName(form.getLastName());
        ticket.setVip(form.isVip());
        ticket.setCreationDate(LocalDateTime.now());

        ticketRepository.save(ticket);
        generatePdf(ticket);

        return ticket;
    }

    private void generatePdf(Ticket ticket) throws IOException, DocumentException {
        String outputFolder = new ClassPathResource("/public/tickets").getURL().getPath() + File.separator + "bilet_" + ticket.getId() + ".pdf";
        System.out.println(outputFolder);
        OutputStream outputStream = new FileOutputStream(outputFolder);

        String html = parsePdfTemplate(ticket);

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);

        outputStream.close();
    }

    private String parsePdfTemplate(Ticket ticket) {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();
        Showing showing = ticket.getShowing();
        Movie movie = showing.getMovie();
        double price = ticket.isVip() ? movie.getPrice() * 1.1: movie.getPrice();
        context.setVariable("type", ticket.isVip() ? "VIP" : "STANDARD");
        context.setVariable("title", movie.getTitle());
        context.setVariable("date", showing.getPlayingDate().format(DateTimeFormatter.ofPattern("dd LLLL yyyy HH:mm")));
        context.setVariable("room", showing.getRoom().getId());
        context.setVariable("name", ticket.getFirstName() + ' ' + ticket.getLastName());
        context.setVariable("price", new DecimalFormat("#.##").format(price / 100));

        return templateEngine.process("templates/ticket", context);
    }
}
