package pl.edu.pwsz.cinema.commerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.pwsz.cinema.commerce.entity.Ticket;

/**
 * Repozytorium do wybierania biletów z bazy
 */
public interface TicketRepository extends JpaRepository<Ticket, Integer> {
}
