INSERT INTO movie (id, title, description, cover, rating, price) VALUES (1, 'Skazani na Shawshank', 'Adaptacja opowiadania Stephena Kinga. Niesłusznie skazany na dożywocie bankier, stara się przetrwać w brutalnym, więziennym świecie.', 'skazani_na_shawshank.jpg', 88, 2499);
INSERT INTO movie (id, title, description, cover, rating, price) VALUES (2, 'Ojciec Chrzestny', 'Opowieść o nowojorskiej rodzinie mafijnej. Starzejący się Don Corleone pragnie przekazać władzę swojemu synowi.', 'ojciec_chrzestny.jpg', 86, 1899);
INSERT INTO movie (id, title, description, cover, rating, price) VALUES (3, 'Pulp Fiction', 'Przemoc i odkupienie w opowieści o dwóch płatnych mordercach pracujących na zlecenie mafii, żonie gangstera, bokserze i parze okradającej ludzi w restauracji.', 'pulp_fiction.jpg', 84, 1599);

INSERT INTO room (id, seats, vip_seats) values (1, 5, 2);
INSERT INTO room (id, seats, vip_seats) values (2, 10, 4);

INSERT INTO showing (id, movie_id, room_id, playing_date) VALUES (1, 1, 1, CURRENT_TIMESTAMP() + 1);
INSERT INTO showing (id, movie_id, room_id, playing_date) VALUES (2, 1, 2, CURRENT_TIMESTAMP() + 1);
INSERT INTO showing (id, movie_id, room_id, playing_date) VALUES (3, 2, 1, CURRENT_TIMESTAMP() + 1);
INSERT INTO showing (id, movie_id, room_id, playing_date) VALUES (4, 2, 2, CURRENT_TIMESTAMP() + 1);
INSERT INTO showing (id, movie_id, room_id, playing_date) VALUES (5, 3, 1, CURRENT_TIMESTAMP() + 1);
INSERT INTO showing (id, movie_id, room_id, playing_date) VALUES (6, 3, 2, CURRENT_TIMESTAMP() + 1);

INSERT INTO ticket (id, showing_id, creation_date, first_name, last_name, is_vip) VALUES (1, 1, CURRENT_TIMESTAMP(), 'Adam', 'Adamski', false);
INSERT INTO ticket (id, showing_id, creation_date, first_name, last_name, is_vip) VALUES (2, 1, CURRENT_TIMESTAMP(), 'Bartek', 'Bartkowski', false);
INSERT INTO ticket (id, showing_id, creation_date, first_name, last_name, is_vip) VALUES (3, 1, CURRENT_TIMESTAMP(), 'Marek', 'Markowski', false);
INSERT INTO ticket (id, showing_id, creation_date, first_name, last_name, is_vip) VALUES (4, 1, CURRENT_TIMESTAMP(), 'Mateusz', 'Mateuszyński', true);
INSERT INTO ticket (id, showing_id, creation_date, first_name, last_name, is_vip) VALUES (5, 1, CURRENT_TIMESTAMP(), 'Darek', 'Darkowski', true);